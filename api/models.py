from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from enum import Enum


class RoleChoice(Enum):
    DA = "Delivery_Agent"
    OC = "Order_Caterer"
    CS = "Customer_Service"
    MN = "Manager"
    PR = "President"


class Employee(models.Model):
    number = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    role = models.CharField(
        max_length=5,
        choices=[(role, role.value) for role in RoleChoice]  # list of Tuple
    )
    account = models.ForeignKey(User, on_delete=models.CASCADE)
    manager = models.ForeignKey(self, null=True, related_name="employee")

    class Meta:
        verbose_name = "Employee"
        verbose_name_plural = "Employees"

    def __str__(self):
        return f"<Employee: {self.name}, Role: {self.role}>"


class Customer(models.Model):
    cid = models.AutoField(primary_key=True)
    cname = models.CharField(max_length=38)
    cemail = models.EmailField()
    account = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = "Customer"
        verbose_name_plural = "Customers"

    def __str__(self):
        if self.account:
            return f"<Customer: {self.cname}, Have an account>"
        return f"<Customer: {self.cname}>"


class Category(models.Model):
    """The class that define a article's category"""
    name = models.CharField(max_length=50)
    parent = models.ForeignKey(self, null=True, related_name="child")

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

    def __str__(self):
        if self.parent:
            return f"<Category: {self.name}, Parent: {self.parent}>"
        return f"<Category: {self.name}>"


class Article(models.Model):
    """An  article"""
    reference = models.CharField(max_length=50, primary_key=True)
    name = models.CharField(max_length=50)
    description = models.TextField()
    unite_price = models.DecimalField(max_digits=10, decimal_places=2)
    vat_rate_100 = models.DecimalField(max_digits=6, decimal_places=4)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Article"
        verbose_name_plural = "Articles"

    def __str__(self):
        return f"<Article: {self,name}>"


class Variant(models.Model):
    """A variant of article"""
    label = models.CharField(max_length=50)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Variant"
        verbose_name_plural = "Variants"

    def __str__(self):
        return f"<Variant: {self.label}>"


class StateChoice(Enum):
    PN = "Pending"
    BP = "Being Processed"
    RD = "Ready"


class Order(models.Model):
    """The order passed by a customer"""
    reference = models.AutoField(primary_key=True)
    date = models.DateTimeField(auto_now_add=True)
    address_end = models.CharField(max_length=50)
    address_end_sup = models.CharField(
        max_length=50, null=True, blank=True)  # address suplement info
    address_road = models.CharField(max_length=50)
    address_geoloc = models.CharField(max_length=50, null=True, blank=True)
    address_zip_code = models.CharField(max_length=50)
    address_city = models.CharField(max_length=50)
    state = models.CharField(
        max_length=5,
        choices=[(state, state.value) for state in StateChoice]
    )
    owner = models.ForeignKey(Customer, on_delete=models.CASCADE)
    shipping = models.ForeignKey(
        Shipping, on_delete=models.SET_NULL, null=True)
    is_shipped = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Order"
        verbose_name_plural = "Orders"

    def __str__(self):
        return f"<Order: {self.reference}, Owner: {self.owner.cname}>"


class Bill(models.Model):
    """The bill that follow the shipping"""
    reference = models.AutoField(primary_key=True)
    date = models.DateTimeField(default=timezone.now)

    class Meta:
        verbose_name = "Bill"
        verbose_name_plural = "Bills"

    def __str__(self):
        return f"<Bill: {self.reference}>"


class Shipping(models.Model):
    """Record informations about shipped article"""
    date = models.DateTimeField(auto_now_add=True)
    agent = models.ForeignKey(Employee, on_delete=models.CASCADE)
    bill = models.OneToOneField(Bill, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Shipping"
        verbose_name_plural = "Shippings"

    def __str__(self):
        return f"<Shipping: {self.id}, Delivery agent: {self.agent}>"
