from django.contrib.auth.models import User

from rest_framework import serializers

from .models import Employee, Customer, Category, Article, Variant
from .models import Order, Bill, Shipping


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'username', 'email', 'is_active',
                  'is_staff', 'is_superuser',)
        model = User


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('number', 'name', 'role', 'manager', )
    model = Employee


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('cid', 'cname', 'email', 'account', )
    model = Customer


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'name', 'parent', )
    model = Category


class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('reference', 'name', 'description', 'unit_price',
                  'vat_rate_100', 'category', )
    model = Article


class VariantSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'label', 'article', )
    model = Variant


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('reference', 'date', 'address_end', 'address_end_sup',
                  'address_road', 'address_geoloc', 'address_zip_code',
                  'address_city', 'state', 'owner', 'shipping', 'is_shipped', )
    model = Order


class BillSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('reference', 'date', )
    model = Bill


class ShippingSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'date', 'agent', 'bill', )
    model = Shipping
